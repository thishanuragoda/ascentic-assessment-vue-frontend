import { createStore } from "vuex";
import auth from "@/store/modules/auth";
import questions from "@/store/modules/questions";
import comments from "@/store/modules/comments";

export default createStore({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    auth,
    questions,
    comments
  },
});
