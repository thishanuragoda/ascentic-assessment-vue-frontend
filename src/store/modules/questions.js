import API from '@/services/api'

export default {
    namespaced: true,
    state: {
        questions: []
    },
    getters: {
        questions: (state) => state.questions
    },
    mutations: {
        SET_QUESTIONS: (state, payload) => {
            state.questions = payload
        },
        REMOVE_QUESTION: (state, payload) => {
            state.quetions = state.questions.filter(q => q.id !== payload)
        }
    },
    actions: {
        async getAll({ commit }, payload) {
            await API.get('/api/questions', payload)
                .then(response => {
                    commit('SET_QUESTIONS', response.data.data)
                })
        },
        async getOne({ commit }, payload) {
            const question = await API.getById('/api/questions', payload)
                .then(response => {
                    commit('SET_QUESTIONS', response.data.data)
                    return response.data.data
                }).catch(error => {
                    console.log('ERROR', error)
                    return null
                })
            return question
        },
        async create(context, payload) {
            return await API.post('/api/questions', payload)
                .then(response => {
                    return response.data
                }).catch(error => {
                    return error.response.data
                })
        },
        async update(context, payload) {
            return await API.update('/api/questions', payload.id, payload)
                .then((response) => {
                    return response.data.success
                }).catch(error => {
                    console.log('ERROR', error)
                    return false
                })
        },
        async delete(context, id) {
            return await API.delete('/api/questions', id)
                .then((response) => {
                    context.dispatch('getAll')
                    console.log('rsdds',response)
                    return true
                })
                .catch(error => {
                    console.log('ERROR', error)
                    return false
                })
        }
    },
}
