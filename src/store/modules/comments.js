// import API from '@/services/api'

import API from "@/services/api";

export default {
    namespaced: true,
    state: {
        comments: []
    },
    getters: {
        comments: state => state.comments
    },
    mutations: {
        SET_COMMENTS: (state, payload) => {
            state.comments = payload
        }
    },
    actions: {
        async getAll({ commit }, payload) {
            await API.get('/api/comments', payload)
                .then(response => {
                    commit('SET_COMMENTS', response.data.data)
                }).catch(error => {
                    console.log('ERROR', error)
                    commit('SET_COMMENTS', null)
                })
        },
        async create(context, payload) {
            return await API.post('/api/comments', payload)
                .then(response => {
                    return response.data
                }).catch(error => {
                    console.log('ERROR', error)
                    return error.response.data
                })
        },
    },
}
