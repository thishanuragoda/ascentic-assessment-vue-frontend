import API from '@/services/api';

export default {
    namespaced: true,
    state: {
        user: null
    },
    getters: {
        user: state => state.user
    },
    mutations: {
        SET_USER: (state, payload) => {
            state.user = payload
        }
    },
    actions: {
        // Send login request to laravel passport
        async login(context, payload) {
            return await API.post('/oauth/token', {
                "grant_type": "password",
                // TODO:: Change this to passport password grant client_id
                "client_id": 2,
                // TODO:: Change this value to passport password grant client_secret
                "client_secret": "MhZwUUvARweis6c2BQ4ojC18mGj0TsFu0j69d1aZ",
                "username": payload.email,
                "password": payload.password,
                "scope": "*"
            }).then(response => {
                // Check if the successful login returned access token
                if (typeof response.data.access_token !== undefined) {
                    // Save access_token in localStorage for future request authentication
                    localStorage.setItem('access_token', response.data.access_token)
                } else {
                    // Remove access_token from localStorage to prevent unauthorized users
                    localStorage.removeItem('access_token')
                }
                return response.data
            }).catch(error => {
                console.log('LOGIN_ERROR', error)
                return error.response
            })
        },
        // Fetch authenticated user from server
        async getUser({ commit }) {
            await API.get('/api/user')
                .then(response => {
                    if(response.data.success) {
                        commit('SET_USER', response.data.data)
                    } else {
                        localStorage.removeItem('access_token')
                        commit('SET_USER', null)
                    }
                }).catch(error => {
                    console.log('AUTHENTICATED_USER_EROR', error)
                    commit('SET_USER', null)
                })
        }
    }
}
