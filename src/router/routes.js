import GuestBody from '@/components/GuestBody';
import UserBody from '@/components/UserBody';

// Import Vue files located in /src/views directory
function page(path) {
    return () => import(`@/views/${path}`).then(m => m.default || m)
}

const routes = [
    { path: '/', redirect: { name: 'dashboard' } },
    {
        path: '/',
        component: GuestBody,
        children: [
            {
                path: "login", name: "login", component: page('auth/Login'),
                meta: { title: 'Login', requiresVisitor: true }
            },
            {
                path: "register", name: "register", component: page('auth/Register'),
                meta: { title: 'Register', requiresVisitor: true }
            },
        ]
    },
    {
        path: '/',
        component: UserBody,
        children: [
            {
                path: '', name: 'dashboard', component: page('Home'),
                meta: { title: 'Dashboard', requiresAuth: true }
            },
            {
                path: 'questions/:questionId', name: 'view-question', component: page('questions/Show'),
                meta: { title: 'Question Details', requiresAuth: true }
            },
            {
                path: "users/:userId/questions/create", name: "create-question", component: page('questions/Create'),
                meta: { title: 'New Question', requiresAuth: true }
            }
        ]
    }
];

export default routes;
