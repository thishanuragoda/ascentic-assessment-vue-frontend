import { createRouter, createWebHistory } from "vue-router";
import routes from './routes'

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

router.beforeEach((to, from, next) => {
  // Update browser title
  if(to.meta.title)document.title = to.meta.title + " | XYZ Company";
  // Handle guest and authenticated routes and redirect appropriately
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // Populate array with Guest Only routes
    const guestOnlyRoutes = ['login', 'register'];
    // Check if current route is a guest only route
    if(!guestOnlyRoutes.includes(to.name) && !localStorage.getItem('access_token')) {
      next({
        name: 'login',
        query: { redirect: to.fullPath }
      })
    } else {
      next()
    }
  } else if(to.matched.some(record => record.meta.requiresVisitor)) {
    // Handle routes that are Guest only
    if(localStorage.getItem('access_token')) {
      // Redirect to dashboard if access_token found in local storage
      next({ name: 'dashboard' })
    } else {
      next()
    }
  } else {
    next()
  }
})

export default router;
