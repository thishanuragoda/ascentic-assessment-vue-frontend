import axios from 'axios'

const AxiosInstance = axios.create({
    //TODO:: Change the backend API endpoint here
    baseURL: 'http://127.0.0.1:8000',
    headers: {
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Authorization": "Bearer " + localStorage.getItem('access_token')
    },
})

export default AxiosInstance;
