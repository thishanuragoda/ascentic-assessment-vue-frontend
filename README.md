# Vue Frontend

## Project setup

After downloading (cloning) the repository, navigate to root folder in terminal and execute following command to download the dependencies. 

```
npm install
```
Next open the `/src/store/modules/auth.js` file and change the password grant `client_id` and `client_secret` values from laravel backend.

Optionally open the `/src/plugins/axios.js` file and change the `baseURL` value to your backend api endpoint.


### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```
